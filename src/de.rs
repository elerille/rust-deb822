use serde::de::{self, DeserializeSeed, IntoDeserializer, MapAccess, SeqAccess, Visitor};
use serde::Deserialize;

use crate::error::{Error, Result};

#[derive(Debug)]
pub struct Deserializer<'de> {
    input: &'de str,
}

impl<'de> Deserializer<'de> {
    pub fn from_str(input: &'de str) -> Self {
        Deserializer { input }
    }
}

pub fn from_str<'a, T>(s: &'a str) -> Result<T>
where
    T: Deserialize<'a>,
{
    let mut deserializer = Deserializer::from_str(s);
    let t = T::deserialize(&mut deserializer)?;

    if deserializer.is_empty() {
        Ok(t)
    } else {
        Err(Error::TrailingCharacters)
    }
}

impl<'de> Deserializer<'de> {
    fn line_is_empty(line: &str) -> bool {
        line.trim_start_matches([' ', '\t']).is_empty()
    }
    fn is_empty(&self) -> bool {
        self.input.is_empty()
    }
    fn is_end_stanza(&mut self) -> bool {
        self.peek_line_not_comment()
            .map(|line| Self::line_is_empty(line))
            .unwrap_or(true)
    }
    fn is_continuation(&mut self) -> bool {
        !self.is_end_stanza() && self.input.starts_with(' ')
    }
    fn next_identifier(&mut self) -> Result<&'de str> {
        self.to_not_comment();
        if self.is_empty() {
            Err(Error::MissingKVLine)
        } else if self.input.starts_with(' ') {
            Err(Error::IdentifierCantBeginBySpace)
        } else {
            match self.input.split_once(':') {
                Some((key, next)) => {
                    if key.contains('\n') {
                        Err(Error::IdentifierCantContainsNewLine)
                    } else {
                        self.input = next;
                        Ok(key)
                    }
                }
                None => Err(Error::RequireColonAfterIdentifier),
            }
        }
    }
    fn next_value(&mut self) -> Vec<&'de str> {
        let mut begin = self.next_line().unwrap_or("");
        if begin.starts_with(' ') {
            begin = &begin[1..];
        }
        let mut v;
        if begin.is_empty() {
            v = vec![];
        } else {
            v = vec![begin];
        }
        while self.is_continuation() {
            v.push(&self.next_line().unwrap()[1..]);
        }
        v
    }
    fn next_line(&mut self) -> Option<&'de str> {
        match self.input.split_once('\n') {
            Some((line, next)) => {
                self.input = next;
                Some(line)
            }
            None => {
                if self.input.is_empty() {
                    None
                } else {
                    let old = self.input;
                    self.input = "";
                    Some(old)
                }
            }
        }
    }
    fn next_comment(&mut self) -> Option<&str> {
        if self.input.starts_with('#') {
            match self.input.split_once('\n') {
                Some((comment, next)) => {
                    self.input = next;
                    Some(&comment[1..])
                }
                None => {
                    let old = self.input;
                    self.input = "";
                    Some(&old[1..])
                }
            }
        } else {
            None
        }
    }
    fn to_next_stanza(&mut self) {
        while self.is_end_stanza() && !self.is_empty() {
            self.next_line();
        }
    }
    fn to_not_comment(&mut self) {
        while self.next_comment().is_some() {}
    }
    fn peek_line(&self) -> Option<&str> {
        match self.input.split_once('\n') {
            Some((key, _)) => Some(key),
            None => {
                if self.input.is_empty() {
                    None
                } else {
                    Some(self.input)
                }
            }
        }
    }
    fn peek_line_not_comment(&mut self) -> Option<&str> {
        self.to_not_comment();
        self.peek_line()
    }
}

impl<'de, 'a> de::Deserializer<'de> for &'a mut Deserializer<'de> {
    type Error = Error;

    fn deserialize_any<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_bool<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_i8<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!();
    }

    fn deserialize_i16<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!();
    }

    fn deserialize_i32<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!();
    }

    fn deserialize_i64<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!();
    }

    fn deserialize_u8<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!();
    }

    fn deserialize_u16<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!();
    }

    fn deserialize_u32<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!();
    }

    fn deserialize_u64<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!();
    }

    fn deserialize_f32<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_f64<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_char<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_str<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        visitor.visit_borrowed_str(self.next_identifier()?)
    }

    fn deserialize_string<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        visitor.visit_borrowed_str(self.next_identifier()?)
    }

    fn deserialize_bytes<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_byte_buf<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_option<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_unit<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_unit_struct<V>(self, _name: &'static str, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_newtype_struct<V>(self, _name: &'static str, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_seq<V>(mut self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let value = visitor.visit_seq(&mut self)?;
        self.is_empty()
            .then_some(value)
            .ok_or(Error::TrailingSequence)
    }

    fn deserialize_tuple<V>(self, _len: usize, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_tuple_struct<V>(
        self,
        _name: &'static str,
        _len: usize,
        _visitor: V,
    ) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_map<V>(mut self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let value = visitor.visit_map(&mut self)?;
        self.is_end_stanza()
            .then_some(value)
            .ok_or(Error::ExpectedStanzaEnd)
    }

    fn deserialize_struct<V>(
        self,
        _name: &'static str,
        _fields: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        // TODO Add case insensitive with fields arg
        self.deserialize_map(visitor)
    }

    fn deserialize_enum<V>(
        self,
        _name: &'static str,
        _variants: &'static [&'static str],
        _visitor: V,
    ) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_identifier<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        visitor.visit_borrowed_str(self.next_identifier()?)
    }

    fn deserialize_ignored_any<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }
}

impl<'de> MapAccess<'de> for Deserializer<'de> {
    type Error = Error;

    fn next_key_seed<K>(&mut self, seed: K) -> Result<Option<K::Value>>
    where
        K: DeserializeSeed<'de>,
    {
        if self.is_end_stanza() {
            Ok(None)
        } else {
            seed.deserialize(self).map(Some)
        }
    }
    fn next_value_seed<V>(&mut self, seed: V) -> Result<V::Value>
    where
        V: DeserializeSeed<'de>,
    {
        seed.deserialize(&mut ValueDeserializer::from_vec_str(self.next_value()))
    }
}
impl<'de> SeqAccess<'de> for Deserializer<'de> {
    type Error = Error;

    fn next_element_seed<T>(&mut self, seed: T) -> Result<Option<T::Value>>
    where
        T: DeserializeSeed<'de>,
    {
        if self.is_empty() {
            Ok(None)
        } else {
            let r = seed.deserialize(&mut *self).map(Some);
            self.to_next_stanza();
            r
        }
    }
}

#[derive(Debug)]
pub struct ValueDeserializer<'de> {
    input: Vec<&'de str>,
}

impl<'de> ValueDeserializer<'de> {
    pub fn from_vec_str(input: Vec<&'de str>) -> Self {
        ValueDeserializer { input }
    }

    fn to_not_empty(&mut self) {
        while !self.input.is_empty() && self.input[0].trim().is_empty() {
            self.input.remove(0);
        }
        if !self.input.is_empty() {
            self.input[0] = self.input[0].trim_start();
        }
    }

    fn is_empty(&mut self) -> bool {
        self.to_not_empty();
        self.input.is_empty()
    }
    fn next_token(&mut self) -> Result<&'de str> {
        if self.is_empty() {
            Err(Error::MissingToken)
        } else {
            match self.input[0].split_once(char::is_whitespace) {
                Some((token, next)) => {
                    self.input[0] = next;
                    Ok(token)
                }
                None => {
                    let old = self.input[0];
                    self.input.remove(0);
                    Ok(old)
                }
            }
        }
    }
}

impl<'de, 'a> de::Deserializer<'de> for &'a mut ValueDeserializer<'de> {
    type Error = Error;

    fn deserialize_any<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_bool<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_i8<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!();
    }

    fn deserialize_i16<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!();
    }

    fn deserialize_i32<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!();
    }

    fn deserialize_i64<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!();
    }

    fn deserialize_u8<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!();
    }

    fn deserialize_u16<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!();
    }

    fn deserialize_u32<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!();
    }

    fn deserialize_u64<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!();
    }

    fn deserialize_f32<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_f64<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_char<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_str<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_string<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let value = self.input.join("\n");
        self.input.clear();
        visitor.visit_string(value)
    }

    fn deserialize_bytes<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_byte_buf<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_option<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_unit<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_unit_struct<V>(self, _name: &'static str, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_newtype_struct<V>(self, _name: &'static str, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_seq<V>(mut self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        let value = visitor.visit_seq(&mut self)?;
        self.input
            .is_empty()
            .then_some(value)
            .ok_or(Error::TrailingSequence)
    }

    fn deserialize_tuple<V>(self, _len: usize, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_tuple_struct<V>(
        self,
        _name: &'static str,
        _len: usize,
        _visitor: V,
    ) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_map<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_struct<V>(
        self,
        _name: &'static str,
        _fields: &'static [&'static str],
        _visitor: V,
    ) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_enum<V>(
        self,
        _name: &'static str,
        _variants: &'static [&'static str],
        visitor: V,
    ) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        visitor.visit_enum(self.next_token()?.into_deserializer())
    }

    fn deserialize_identifier<V>(self, _visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        unimplemented!()
    }

    fn deserialize_ignored_any<V>(self, visitor: V) -> Result<V::Value>
    where
        V: Visitor<'de>,
    {
        self.deserialize_string(visitor)
    }
}

impl<'de> SeqAccess<'de> for ValueDeserializer<'de> {
    type Error = Error;

    fn next_element_seed<T>(&mut self, seed: T) -> Result<Option<T::Value>>
    where
        T: DeserializeSeed<'de>,
    {
        if self.is_empty() {
            Ok(None)
        } else {
            seed.deserialize(&mut *self).map(Some)
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::collections::HashMap;

    #[derive(Deserialize, PartialEq, Debug)]
    struct Point {
        x: String,
        y: String,
    }

    #[test]
    fn test_simple_struct() {
        let deb822 = "x: my-x\ny: MY YYYYY";
        let pt_deb822: Point = from_str(deb822).unwrap();
        let expected = Point {
            x: "my-x".into(),
            y: "MY YYYYY".into(),
        };
        assert_eq!(expected, pt_deb822);
    }

    #[test]
    fn test_vec_struct() {
        let deb822 = "x: my-x\ny: MY YYYYY\n\ny: zyx\n#adf\n 123\nx: abc\n\n";
        let pt_deb822: Vec<Point> = from_str(deb822).unwrap();
        let expected1 = Point {
            x: "my-x".into(),
            y: "MY YYYYY".into(),
        };
        let expected2 = Point {
            x: "abc".into(),
            y: "zyx\n123".into(),
        };
        assert_eq!(vec![expected1, expected2], pt_deb822);
    }

    #[test]
    fn test_simple_map() {
        let deb822 = "x: my-x\ny: MY YYYYY";
        let pt_deb822: HashMap<String, String> = from_str(deb822).unwrap();
        let expected: HashMap<String, String> =
            [("x".into(), "my-x".into()), ("y".into(), "MY YYYYY".into())]
                .into_iter()
                .collect();
        assert_eq!(expected, pt_deb822);

        let pt_deb822: HashMap<&str, String> = from_str(deb822).unwrap();
        let expected: HashMap<&str, String> =
            [("x".into(), "my-x".into()), ("y".into(), "MY YYYYY".into())]
                .into_iter()
                .collect();
        assert_eq!(expected, pt_deb822);
    }

    #[test]
    fn test_simple_enum() {
        #[derive(Deserialize, PartialEq, Debug)]
        enum Val {
            A,
            B,
            C,
        }

        let deb822 = "x: A B C";
        let pt_deb822: HashMap<String, Val> = from_str(deb822).unwrap();
        let expected: HashMap<String, Val> = [("x".into(), Val::A)].into_iter().collect();
        assert_eq!(expected, pt_deb822);
    }

    #[test]
    fn test_simple_seq_enum() {
        #[derive(Deserialize, PartialEq, Debug)]
        enum Val {
            A,
            B,
            C,
        }
        #[derive(Deserialize, PartialEq, Debug)]
        struct ListX {
            x: Vec<Val>,
        }

        let deb822 = "x: A B C";
        let pt_deb822: ListX = from_str(deb822).unwrap();
        let expected = ListX {
            x: vec![Val::A, Val::B, Val::C],
        };
        assert_eq!(expected, pt_deb822);
    }

    #[test]
    fn test_ignore_supplement_field() {
        let deb822 = "x: my-x\ny: MY YYYYY\nz: New Field";
        let pt_deb822: Point = from_str(deb822).unwrap();
        let expected = Point {
            x: "my-x".into(),
            y: "MY YYYYY".into(),
        };
        assert_eq!(expected, pt_deb822);
    }
}
