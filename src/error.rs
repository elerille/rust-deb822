use std;
use std::fmt::{self, Display};

use serde::{de, ser};

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Debug, PartialEq)]
pub enum Error {
    Message(String),
    Deb822Message(String),
    TrailingCharacters,
    TrailingSequence,
    ExpectedStanzaEnd,
    IdentifierCantBeginBySpace,
    IdentifierCantContainsNewLine,
    IdentifierCantContainsColon,
    RequireColonAfterIdentifier,
    MissingKVLine,
    MissingToken,
    IgnoredEndStanza,
}

impl ser::Error for Error {
    fn custom<T>(msg: T) -> Self
    where
        T: Display,
    {
        Error::Message(msg.to_string())
    }
}

impl de::Error for Error {
    fn custom<T>(msg: T) -> Self
    where
        T: Display,
    {
        Error::Message(msg.to_string())
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::Message(msg) => write!(f, "{msg}"),
            Self::Deb822Message(msg) => write!(f, "Deb822 Error {msg}"),
            err => write!(f, "Deb822 Error {err:?}"),
        }
    }
}

impl std::error::Error for Error {}
